var canvas ;
var ctx ;
var PushArray = [];
var step = -1;
var flag = 0;
var mousePos;
var saveimage;
var dynamic_mousePos;
var nowcolor;

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };   
  };

function mouseMove(evt) {

    if(flag == 1 || flag == 0)
    {
        if(flag != 0)
            ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineWidth = document.getElementById("brush_size").value;
        dynamic_mousePos = getMousePos(canvas, evt);
        ctx.lineTo(dynamic_mousePos.x, dynamic_mousePos.y);
        ctx.stroke();
    } 
    if(flag == 2)
    {
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.fillStyle = document.getElementById("color").value; 
        ctx.lineWidth = document.getElementById("brush_size").value;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.putImageData(saveimage, 0, 0);
        ctx.fillRect(mousePos.x, mousePos.y, event.offsetX -mousePos.x, event.offsetY -mousePos.y); 
    }
    if(flag == 3)
    {
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.fillStyle = document.getElementById("color").value;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.putImageData(saveimage, 0, 0);
        ctx.beginPath();
        var radius = ((event.offsetX-mousePos.x)**2+(event.offsetY-mousePos.y)**2)**(1/2);
        ctx.arc(mousePos.x, mousePos.y, radius, 0, 360*Math.PI/180);
        ctx.stroke();
        ctx.fill();        
    }
    if(flag == 4)
    {
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.fillStyle = document.getElementById("color").value;
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.putImageData(saveimage, 0, 0);
        ctx.beginPath();
        var wide = mousePos.x-(event.offsetX);
        ctx.moveTo(mousePos.x, mousePos.y);
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.lineTo(mousePos.x+wide, event.offsetY);
        ctx.fill();    
    }
    if(flag == 5)
    {
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.fillStyle = document.getElementById("color").value;
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.putImageData(saveimage, 0, 0);
        ctx.beginPath();
        var wide = mousePos.y-(event.offsetY);
        ctx.moveTo(mousePos.x, mousePos.y);
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.lineTo(event.offsetX, mousePos.y+wide);
        ctx.fill();    
    }
    if(flag == 8)
    {
        ctx.lineWidth = document.getElementById("brush_size").value;
        gradient = ctx.createLinearGradient(0, 0, 800, 0);
        gradient.addColorStop("0", "red");
        gradient.addColorStop("0.3", "magenta");
        gradient.addColorStop("0.6", "yellow");
        gradient.addColorStop("0.8", 'green');
        gradient.addColorStop("0.9", 'blue');
        gradient.addColorStop("1", 'purple');    
        ctx.strokeStyle = gradient;
        dynamic_mousePos = getMousePos(canvas, evt);
        ctx.lineTo(dynamic_mousePos.x, dynamic_mousePos.y);
        ctx.stroke();        
    }
}

function Push() {
    step++;
    if (step < PushArray.length) { PushArray.length = step; }
    PushArray.push(document.getElementById('canvas').toDataURL());
}

function undo() {
    if (step > 0) {
        step--;
        var canvasPic = new Image();
        ctx.clearRect(0, 0, canvas.width, canvas.height);       
        canvasPic.onload = function () { 
            ctx.drawImage(canvasPic, 0, 0); 
            saveimage = ctx.getImageData(0, 0, canvas.width, canvas.height); 
        }
        canvasPic.src = PushArray[step];//放下面onload才會執行
    }
    else
    {
        ctx.clearRect(0, 0, canvas.width, canvas.height); 
        saveimage = ctx.getImageData(0, 0, canvas.width, canvas.height);
        if(step == 0)
            step--; 
    }
}

function redo() {
    if (step < PushArray.length-1) {
        step++;
        var canvasPic = new Image();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () { 
            ctx.drawImage(canvasPic, 0, 0); 
            saveimage = ctx.getImageData(0, 0, canvas.width, canvas.height);}
        canvasPic.src = PushArray[step];
    }
}

function readURL(input){

    if(input.files && input.files[0]){
  
      var imageTagID = input.getAttribute("targetID");
  
      var reader = new FileReader();
  
      reader.onload = function (e) {
  
         var img = document.getElementById(imageTagID);
  
         img.setAttribute("src", e.target.result)
  
      }
  
      reader.readAsDataURL(input.files[0]);
  
    }
}

function flag6() {
    flag = 6; 
}

function fonttype(){
    var x=document.getElementById("myselect");
    var y=document.getElementById("myselect2");    
    var txt_face = y.value;    
    var txt_size = x.value;
    ctx.font = txt_size + "px " + txt_face;
}

function init() {
    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 15;
    saveimage = ctx.getImageData(0, 0, canvas.width, canvas.height); 

    canvas.addEventListener('mousedown', function(evt) {
        if(flag == 6)
        {
            var img = document.getElementById("preview_progressbarTW_img");
            ctx.drawImage(img, event.offsetX, event.offsetY);
        }
        mousePos = getMousePos(canvas, evt);
        ctx.beginPath();
        ctx.moveTo(mousePos.x, mousePos.y);
        evt.preventDefault();
        if(flag == 7)
        {
            var txt = document.getElementById("mytext");
            ctx.strokeStyle = document.getElementById("color").value;
            ctx.fillStyle = document.getElementById("color").value;             
            fonttype();
            ctx.fillText(txt.value,event.offsetX,event.offsetY);
        }
        canvas.addEventListener('mousemove', mouseMove, false);
       });
      
      canvas.addEventListener('mouseup', function() {
        canvas.removeEventListener('mousemove', mouseMove, false);
        saveimage = ctx.getImageData(0, 0, canvas.width, canvas.height);
        Push();
      }, false);
       
      document.getElementById('reset').addEventListener('click', function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        for( step = PushArray.length-1;step > -1; step--){
            PushArray.pop();
        }
        saveimage = ctx.getImageData(0, 0, canvas.width, canvas.height);   
      }, false);

      document.getElementById('paint').addEventListener('click', function() {
        flag = 1;
        ctx.strokeStyle = document.getElementById("color").value;
        ctx.lineWidth = document.getElementById("brush_size").value; 
         canvas.style.cursor = "url('paint.png'),auto"; 
      }, false);
      
      document.getElementById('eraser').addEventListener('click', function() {
          ctx.strokeStyle = 'white';
          ctx.lineWidth = document.getElementById("brush_size").value;
          canvas.style.cursor = "url('eraser.png'),auto";   
          flag = 0;
        }, false);

        document.getElementById('rectangle').addEventListener('click', function() {
            flag = 2;
            canvas.style.cursor = "url('rectangle.png'),auto";  
        }, false);

        document.getElementById('circle').addEventListener('click', function() {
             flag = 3;
             canvas.style.cursor = "url('circle.png'),auto"; 
        }, false);

        document.getElementById('triangle').addEventListener('click', function() {
            flag = 4;
            canvas.style.cursor = "url('triangle.png'),auto";             
        }, false);
        
        document.getElementById('triangle2').addEventListener('click', function() {
            flag = 5;
            canvas.style.cursor = "url('triangle2.png'),auto";   
        }, false);

        document.getElementById('text').addEventListener('click', function() {
            flag = 7;
            canvas.style.cursor = "url('text1.png'),auto";   
        }, false);
 
        document.getElementById('rainbow').addEventListener('click', function() {
            flag = 8;
            gradient = ctx.createLinearGradient(0, 0, 300, 0);
            gradient.addColorStop("0", "magenta");
            gradient.addColorStop("0.5", "blue");
            gradient.addColorStop("1.0", "red");
            ctx.strokeStyle = gradient;
            canvas.style.cursor = "url('spaint.png'),auto"; 
        }, false);
    }





