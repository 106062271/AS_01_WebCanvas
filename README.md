# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
* 功能
    * 筆刷
      用法: 點一下筆刷(鼠標移動上面會有title)圖示,並移動畫布上想要畫的點,
          mousedown,並開始mousemove,在畫布內mouseup。
      效果: 路徑會出現線，並當鼠標移動到畫布內鼠標會更換成筆刷圖示
    * 橡皮擦
      用法: 點一下橡皮擦(鼠標移動上面會有title)圖示,並移動畫布上想要擦掉的點,
          mousedown,並開始mousemove,在畫布內mouseup。
      效果: 路徑上的東西會被清除，並當鼠標移動到畫布內鼠標會更換成橡皮擦圖示
    * 清空畫布
      用法: 點一下垃圾桶(鼠標移動上面會有title)圖示
      效果: 畫布上的所有東西會被清除
    * 復原/還原
      用法: 點一下復原/不復原(鼠標移動上面會有title)圖示
      效果: 復原點一下會回復上一個動作前的畫面，不復原會回復復原前的畫面    
    * 長方形/圓形/三角形/左右三角形
      用法: 點一下長方形/圓形/三角形/左右三角形圖示，並移動畫布上想要畫的點,
          mousedown,並開始mousemove,在畫布內mouseup。
      效果: 起點跟終點會是其中的頂點，並當鼠標移動到畫布內鼠標會更換成其代表的圖示
    * 彩虹筆刷
      用法: 點一下筆刷(鼠標移動上面會有title)圖示,並移動畫布上想要畫的點,
          mousedown,並開始mousemove,在畫布內mouseup。
      效果: 路徑會出現遞進顏色的彩虹線，並當鼠標移動到畫布內鼠標會更換成彩虹筆刷圖示
    * 調整筆刷/彩虹刷/橡皮擦大小
      用法: 拉桿子調整左小右大
      效果: 改變筆刷橡皮擦彩虹刷大小
    * 換筆刷顏色
      用法:點顏色圖示並更換想要的顏色
      效果:改變長方形/圓形/三角形/左右三角形/筆刷/打字貼在畫布的顏色
    * 上傳檔案並貼在畫布上
      用法:點上傳檔案鍵並選擇圖案類的檔案上傳，並在畫布內選擇要貼上的位置，用點擊的方式
      效果:上傳檔案並貼在畫布上
    * 下載畫布檔案
      用法:點選下載圖示
      效果:下載畫部內容檔名是img.png
    * 打字並貼在畫布上
      用法:在輸入文字的空格內打文字並選擇大小/字型,並在畫布內選擇要貼上的位置，用點擊的方式
      效果:打字並貼在畫布上
    * 調整字的大小/字形
      用法:點擊選單並選擇大小字形
      效果:改變字型大小
